var OAuthUrl = "https://accounts.google.com/o/oauth2/auth";
var googleClientId = "160245068190-l6a144iqv0vv6t5q42asnikhs6aelmif.apps.googleusercontent.com";

// local config
var redirectGmailUrl = "http://localhost:9001/desktop/integration/redirectGmail.html";                        

// qa config
var redirectGmailUrl = "https://qa.salesbox.com/desktop/integration/redirectGmail.html";                        
var serviceUrl = "https://production-qa.salesbox.com/enterprise-v3.2";

// production config
//var redirectGmailUrl = "https://go.salesbox.com/desktop/integration/redirectGmail.html";                        
//var serviceUrl = "https://go.salesbox.com/enterprise-v3.2";

var gmailScope = ['https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/script.external_request',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.apps.readonly',
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.labels'
];

var popupWindow = function (url, title, windowWidth, windowHeight) {
    console.log(url);
    var documentElement = document.documentElement;
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

    var width = window.innerWidth || documentElement.clientWidth || screen.width;
    var height = window.innerHeight || documentElement.clientHeight || screen.height;

    var left = ((width - windowWidth) / 2) + dualScreenLeft;
    var top = ((height - windowHeight) / 2) + dualScreenTop;
    return window.open(
        url, title,
        "resizeable=true,height=" + windowHeight + ",width=" + windowWidth + ",left=" + left + ",top=" + top
    );
};

document.addEventListener('DOMContentLoaded', function() {
    $("#btnLogin").on("click", function(event) {
        var username = $("#username").val();
        var password = $("#password").val();
        var hasP = CryptoJS.MD5(password).toString();
        var obj = {
            'username': username,
            'hashPassword': hasP,
            'webPlatform': false,
            'deviceToken': "WEB_TOKEN",
            'version': "3.0"
        };
        $.ajax({
            type: "POST",
            url: serviceUrl + "/user/login",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function(json) {
                if (json.userDTO) {
                    console.log(json.userDTO);

                    chrome.runtime.sendMessage({
                        loggedIn: true,
                        currentUserId: json.userDTO.token
                    });
                    $("#lform").hide();
                    $("#llogin").show();
                    // Open a popup to get gmail code
                    
                    popupWindow(OAuthUrl + '?response_type=code&client_id=' + googleClientId + '&redirect_uri=' + encodeURIComponent(redirectGmailUrl) + "&access_type=offline&approval_prompt=force&" +
                    "&state=" + json.userDTO.token + "&scope=profile " + gmailScope.join(' '), 'Google', 600, 600);
                }
            },
            statusCode: {
                500: function(error) {
                    alert("Invalid Username");
					console.log(error);
                    localStorage.removeItem('token');
                    chrome.runtime.sendMessage({
                        loggedIn: false,
                        currentUserId: "00000"
                    });
                    $("#loginFail").show();
                }
            },
            error: function(e) {
                console.log(e);
            },
        });

    });



    $("#btnLogout").on("click", function(event) {
        $("#id-login").hide();
        $("#id-logout").show();
        chrome.runtime.sendMessage({
            loggedOut: true,
            currentUserId: "0000"
        });
    });


$("#btnNewUser").on("click", function(event) {
    window.open('https://go.salesbox.com/desktop/#/login?mode=register');
});

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            greeting: "hello"
        }, function(response) {

        });
    });

});
window.addEventListener("message", function (event) {
 
    if (event.source != window) return;

    if (event.data.message == "loggedIn") {
        chrome.runtime.sendMessage({
          loggedIn: true,
          currentUserId: event.data.currentUserId,
          pluginToken: event.data.pluginToken
        });
    } else if (event.data.message == "loggedOut") {
        chrome.runtime.sendMessage({
          loggedOut: true
        });
    } else if (event.data.message == "currentList") {
        chrome.runtime.sendMessage({
          setCurrentList: true,
          listId: event.data.listId
        });
    }
});

window.postMessage({message: 'extension_installed'}, "*");
var serviceUrl = "https://production-qa.salesbox.com/";
var StandardProfileResult = function(domWrapper) {
    this.link = function() {
        return $(domWrapper).find("a.title").get(0).href;
    };
    this.name = function() {
        return $(domWrapper).find("a.title").text();
    };
    this.headline = function() {
        return $(domWrapper).find(".bd .description").text();
    };
    this.img = function() {
        return $(domWrapper).find(".entity-img").attr("src");
    };
    this.demographicLocation = function() {
        return $(domWrapper).find(".demographic .separator bdi").text();
    };
    this.industry = function() {
        return $(domWrapper).find(".demographic dd:eq(1)").text();
    };
};
var StandardCompanyResult = function(li) {
    this.id = function() {
        return $(li).data("li-entity-id");
    };
    this.link = function() {
        return $(li).find("a.title").get(0).href;
    };
    this.name = function() {
        return $(li).find("a.title").text();
    };
    this.headline = function() {
        return $(li).find(".bd .description").text();
    };
    this.img = function() {
        return $(li).find(".entity-img").attr("src");
    };
    this.demographicLocation = function() {
        return $(li).find(".demographic .separator bdi").text();
    };
    this.size = function() {
        return $(li).find(".demographic dd:eq(1)").text();
    };

};

var NavigatorProfileResult = function(domWrapper) {
    this.link = function() {
        return $(domWrapper).find(".name a").first().attr("href");
    };
    this.name = function() {
        return $(domWrapper).find(".name a").first().text();
    };
    this.headline = function() {
        return $(domWrapper).find(".headline").text();
    };
    this.img = function() {
        return $(domWrapper).find(".image").attr("src");
    };
    this.demographicLocation = function() {
        return $(domWrapper).find(".demographic .separator bdi").text();
    };
    this.industry = function() {
        return $(domWrapper).find(".demographic dd:eq(1)").text();
    };

};
var NavigatorCompanyResult = function(li) {
    this.id = function() {
        return $(li).data("li-entity-id");
    };
    this.link = function() {
        return $(li).find(".name a").first().attr("href");
    };
    this.name = function() {
        return $(li).find(".name a").first().text();
    };
    this.headline = function() {
        return $(li).find(".headline").text();
    };
    this.img = function() {
        return $(li).find(".account-img").attr("src");
    };
    this.demographicLocation = function() {
        return $(li).find(".demographic .separator bdi").text();
    };
    this.size = function() {
        return $(li).find(".demographic dd:eq(1)").text();
    };
};

var NavigatorAccountResult = function(li) {
    this.id = function() {
        return $(li).find("input[type='checkbox']:first").data("id");
    };
    this.link = function() {
        return $(li).find(".account-name").first().attr("href");
    };
    this.name = function() {
        return $(li).find(".account-name").first().text();
    };
    this.headline = function() {
        return $(li).find(".industry").text().substring(1);
    };
    this.img = function() {
        return $(li).find(".account-img").attr("src");
    };
};

var RecruiterProfileResult = function(domWrapper) {
    this.link = function() {
        var url = $(domWrapper).find(".name a").get(0).href;
        var profile_id = url.match(/cap\/people\/show\/(\d*)\?/)[1];
        var auth_token = url.match(/authToken=(.*?)&/)[1];

        return "https://www.linkedin.com/profile/view?id=" + profile_id + "&authType=CAP&authToken=" + auth_token;
    };
    this.name = function() {
        return $(domWrapper).find(".name a").text();
    };
    this.headline = function() {
        return $(domWrapper).find(".title").text();
    };
    this.img = function() {
        return $(domWrapper).find(".picture-link img").attr("src");
    };
};

var ProfileElementResult = StandardProfileResult;
var CompanyElementResult = StandardCompanyResult;
var peopleContainerSelector = "#results-container";
var companyContainerSelector = "#results-container";
var peopleResultSelector = "li.people";
var companyResultSelector = "li.company";

if (window.location.href.match(/\/sales\/search/) !== null) {
    alert('yes');
    ProfileElementResult = NavigatorProfileResult;
    CompanyElementResult = NavigatorCompanyResult;
    peopleContainerSelector = "#people-results";
    companyContainerSelector = "#companies-results";

    peopleResultSelector = "ul.results li.entity";
    companyResultSelector = "ul.results li.entity";
}
if (window.location.href.match(/\/sales\/accounts/) !== null) {
    CompanyElementResult = NavigatorAccountResult;
    peopleContainerSelector = null;
    companyContainerSelector = "#saved-accounts";

    peopleResultSelector = null;
    companyResultSelector = "ul.entity-list li.account";
}
if (window.location.href.match(/\/cap\/peopleSearch/) !== null) {
    ProfileElementResult = RecruiterProfileResult;

    peopleContainerSelector = "#content";
    companyContainerSelector = null;

    peopleResultSelector = "#results li.results-item";
    companyResultSelector = null;
}

/// ProfileElement Start ///

var ProfileElement = function(domWrapper) {
    this.domWrapper = domWrapper;
    var configuration = new ProfileElementResult(domWrapper);
    this.link = configuration.link();
    this.name = configuration.name();
    this.headline = configuration.headline();
    this.demographicLocation = configuration.demographicLocation();
    this.industry = configuration.industry();
    this.img = configuration.img();
    this.button =
        this.uid = 'linkedin_profile-' + getUidFromUrl(this.link);
    this.el = null;
};

ProfileElement.prototype = {
    getElement: function() {
        this.el = $(
            "<div class='prospector-result' data-uid='" + this.uid + "'>" +
            "<div class='prospector-search-add prospector-search-person'><a href='" + this.link + "' data-name='" + this.name + "' data-headline='" + this.headline + "' data-location='" + this.demographicLocation + "' data-industry='" + this.industry + "' data-image='" + this.img + "'>Add</a></div>" +
            "<a href='" + this.link + "'><img src='" + this.img + "'></a>" +
            "<a href='" + this.link + "'><span class='prospector-result-name'>" + this.name + "</span></a>" +
            "<span class='prospector-result-headline'>" + this.headline + "</span>" +
            "</div>"
        );
        return this.el;
    }
};

var profileElements = [];

function getProfileElementByUid(uid) {
    var profile = null;
    $.each(profileElements, function(index, element) {
        if (element.uid == uid) {
            profile = element;
            return false;
        }
    });
    return profile;
}

function getAllProfileUids() {
    return $.map(profileElements, function(element) {
        return element.uid;
    });
}

/// ProfileElement End ///

/// CompanyElement Start ///

var CompanyElement = function(domWrapper) {
    this.domWrapper = domWrapper;
    var configuration = new CompanyElementResult(domWrapper);
    this.link = configuration.link();
    this.name = configuration.name();
    this.headline = configuration.headline();
    this.img = configuration.img();
    this.button =
        this.id = configuration.id();
    this.demographicLocation = configuration.demographicLocation();
    this.size = configuration.size();
    this.el = null;
};

CompanyElement.prototype = {
    getElement: function() {
        this.el = $(
            "<div class='prospector-result' data-uid='" + this.id + "'>" +
            "<div class='prospector-search-add prospector-search-company'><a href='" + this.link + "' data-name='" + this.name + "' data-headline='" + this.headline + "' data-location='" + this.demographicLocation + "' data-size='" + this.size + "' data-image='" + this.img + "'>Add</a></div>" +
            "<a href='" + this.link + "'><img src='" + this.img + "'></a>" +
            "<a href='" + this.link + "'><span class='prospector-result-name'>" + this.name + "</span></a>" +
            "<span class='prospector-result-headline'>" + this.headline + "</span>" +
            "<span class='prospector-result-headline'>" + this.demographicLocation + "</span>" +
            "</div>"
        );
        return this.el;
    }
};

var companyElements = [];

function getCompanyElementById(id) {
    var company = null;
    $.each(companyElements, function(index, element) {
        if (element.id == id) {
            company = element;
            return false;
        }
    });
    return company;
}

function getAllCompanyIds() {
    return $.map(companyElements, function(element) {
        return element.id;
    });
}

/// CompanyElement End ///

function getUidFromUrl(url) {
    var matches = url.match(/id\=(\d+)/);
    if (!matches) {
        // sales navigator url
        matches = url.match(/\/sales\/profile\/(\d+)/);
    }

    if (matches) {
        return matches[1];
    }

    return null;
}


function displayModal(content) {
    if ($("#prospector-modal-bg").length > 0) return;

    $("body").append("<div id='prospector-modal-bg'></div>");
    $("#prospector-modal-bg").append("<div id='prospector-modal'></div>");

    $("#prospector-modal").html("\
      <h2>" + content['headline'] + "</h2>\
      <p>" + content['body'] + "</p>\
      <p><a class='add-to-prospector' href='" + content['linkURL'] + "' target='_blank'>" + content['linkText'] + "</a>\
         <a id='prospector-modal-close' href='#'>Close</a></p>\
  ");

    $("#prospector-modal").addClass("in");

    $("#prospector-modal-close").on("click", function(e) {
        $("#prospector-modal").removeClass("in");
        setTimeout(function() {
            $("#prospector-modal-bg").fadeOut(200, function() {
                $(this).remove();
                //window.location.reload();
            });
        }, 500);
    })
}



function displayErrorLoginModal() {
    displayModal({
        headline: "Oops!",
        body: "You are not logged in to Salesbox.",
        linkURL: "http://salesbox.com/",
        linkText: "Support"
    });
}


function debugLog(message) {
    if (environment == "development") {
        console.log(message);
    }
}

////////////////////////////////////////////////////////////////////////////////

var environment = null;
var featureFlags = {};
var currentList = null;
var pluginUrl = null;
var pluginToken = null;
var pluginGuid = null;



$(document).ready(function() {
    var loadBox = true;
    loadBox = loadBox && !window.location.href.match(/\/sales\/accounts\/insights/);
    if (loadBox) {
        var isCompanyVisible = $(companyContainerSelector).is(".hidden") === false;
        var isPeopleVisible = $(peopleContainerSelector).is(".hidden") === false;
        var show_buttons = isPeopleVisible && $(peopleContainerSelector).find(peopleResultSelector).length > 0;
        var show_company_buttons = isCompanyVisible && $(companyContainerSelector).find(companyResultSelector).length > 0;
        if (show_buttons) {
            loadProspectorBox();
        }
        if (show_company_buttons) {
            loadProspectorBox();
        }
    }
});
var prospector_box = $(
    "<div id='prospector-box' class='show'>" +
    "<div><a href='http://www.go.salesbox.com/' target='_blank' class='salebox-logo'></a>" +
    "<a href='#' id='hide-prospector-box'>-</a> <a href='#' id='hide-prospector-remove'>x</a></div>" +
    "<div id='prospector-results'></div>" +
    "<div id='prospector-buttons'></div>" +
    "</div>"
);
$("body").prepend("<div id='prospector-box-toggle'> <a href = '#' class='sales-box-icon' ></a></div>");
var loadProspectorBox = function() {
    /* Build Prospector Box */
    $("body").append(prospector_box);
    buildButtons();
    /* Box Toggle */
    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
            chrome.storage.local.set({
                'closebox': 'true'
            });
            chrome.storage.local.set({
                'minimizebox': 'false'
            });
        });
    /* Build Prospector Box */
    chrome.storage.local.get(["minimizebox", "closebox"], function(result) {
        var closebox = result.closebox;
        var minimizebox = result.minimizebox;
        $("body").append(prospector_box);
        console.log(result);
        if (closebox == "true") {
            $("#prospector-box").hide();
            $("#prospector-box").removeClass('show');
        }
        if (minimizebox == "true") {
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
        } else {
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
        }
        $("#hide-prospector-box").on("click", function(event) {
            event.preventDefault();

            prospector_box.fadeOut();
            $("#prospector-box").removeClass('show');
            $("#prospector-box").hide();
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
            chrome.runtime.sendMessage({
                minimizebox: "minimizebox"
            });
        });
        $("#hide-prospector-remove").on("click", function(event) {
            event.preventDefault();

            prospector_box.fadeOut();
            $("#prospector-box").hide();
            $("#prospector-box").removeClass('show');
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
            chrome.runtime.sendMessage({
                closebox: "closebox"
            });
        });
        $("#prospector-box-toggle a").on("click", function(event) {
            event.preventDefault();
            chrome.runtime.sendMessage({
                toggle: "toggle"
            });
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
            $("#prospector-box").show();
            $("#prospector-box").addClass('show');
            prospector_box.fadeIn();
        });
    });
};


var buildButtons = _.debounce(function() {
    var isPeopleVisible = $(peopleContainerSelector).is(".hidden") === false;
    var show_buttons = isPeopleVisible && $(peopleContainerSelector).find(peopleResultSelector).length > 0;
    var isCompanyVisible = $(companyContainerSelector).is(".hidden") === false;
    var show_company_buttons = isCompanyVisible && $(companyContainerSelector).find(companyResultSelector).length > 0;
    $("#prospector-results").html("");
    if (show_buttons) {
        var results_container = $(peopleContainerSelector);
        profileElements = [];
        results_container.find(peopleResultSelector).each(function(i, li) {
            var profileElement = new ProfileElement(li);
            //console.log(profileElement.link);
            $("#prospector-results").append(profileElement.getElement());
            //$.get(profileElement.link, function(data){
            //console.log($(data).find(".profile-card-extras .public-profile a").attr("href"));		
            //});
            profileElements.push(profileElement);
        });
    } else if (show_company_buttons) {
        var results_container = $(companyContainerSelector);
        companyElements = [];
        results_container.find(companyResultSelector).each(function(i, li) {
            var companyElement = new CompanyElement(li);
            //console.log(profileElement.link);
            $("#prospector-results").append(companyElement.getElement());
            //$.get(profileElement.link, function(data){
            //console.log($(data).find(".profile-card-extras .public-profile a").attr("href"));		
            //});
            companyElements.push(companyElement);
        });
    }

}, 500);

var observer = new window.MutationObserver(function() {
    debugLog("New results!");
    buildButtons();
});
if (peopleContainerSelector !== null) {
    observer.observe($(peopleContainerSelector).get(0), {
        childList: true,
        subtree: true
    });
}


prospector_box.on("click", ".prospector-search-person a", function(event) {
    var that = this;
    event.preventDefault();
    event.stopPropagation();
    chrome.storage.local.get(["loginInd", "token"], function(result) {
        loginInd = result.loginInd;
        if (loginInd == "false") {
            displayErrorLoginModal();
        } else {

            //Call API to add prospect	
            var nameAll = $('<div/>').html(that.outerHTML).contents().attr("data-name");
            var headline = $('<div/>').html(that.outerHTML).contents().attr("data-headline");
            var location = $('<div/>').html(that.outerHTML).contents().attr("data-location");
            var industry = $('<div/>').html(that.outerHTML).contents().attr("data-industry");
            var href = $(that).attr('href');
            var image = $('<div/>').html(that.outerHTML).contents().attr('data-image');

            var city = '',
                country = '',
                title = '',
                company = '';
            if (location.split(",").length > 1) {
                city = location.split(",")[0];
                country = location.split(",")[1];
            }
            if (headline.split(" at ").length > 1) {
                title = headline.split(" at ")[0];
                company = headline.split(" at ")[1];
            }
            
            $.ajax({
                type: "Get",
                url: serviceUrl+ "administration-v3.2/workData/workData/industries?token=" + result.token,
                success: function(data) {
                    var industryObject = {};
                    $.each(data.workDataOrganisationDTOList, function(i, obj) {
                        if (obj.name === industry) industryObject = obj;
                    });
                    var avatarImage = '';
                    // upload image
                    toDataUrl(image, function(base64Img) {
                        var dd = getdata(base64Img);
                        var apiData = new FormData();
                        apiData.append("avatar", dd);
                        $.ajax({
                            type: "POST",
                            url: serviceUrl + "document-v3.2/photo/uploadAvatar?token=" + result.token,
                            data: apiData,
                            mimeType: "multipart/form-data",
                            dataType: 'json',
                            contentType: false,
                            async: true,
                            processData: false,
                            success: function(dataCheck) {
                                avatarImage = dataCheck.avatar;
                                var compObj = {
                                    "name": company,
                                    "mediaType": "LINKEDIN",
                                    "additionalEmailList": [],
                                    "additionalPhoneList": [],
                                    "isPrivate": false,
                                    "isChanged": false,
                                    "participantList": [],
                                    "numberGoalsMeeting": 0
                                };
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    contentType: "application/json",
                                    async: false,
                                    data: JSON.stringify(compObj),
                                    url: serviceUrl + "organisation-v3.2/add?token=" + result.token,
                                    success: function(data) {

                                        if (data.uuid)
                                            orgid = data.uuid;
                                        var obj = {
                                            "firstName": nameAll.split(" ")[0],
                                            "lastName": nameAll.split(" ")[1],
                                            "avatar": avatarImage,
                                            "organisationId": orgid,                                            
                                            "discProfile": "NONE",
                                            "mediaType": "MANUAL",
                                            "additionalEmailList": [],
                                            "additionalPhoneList": [],
                                            "relationship": "YELLOW",
                                            "participantList": []
                                        };
										if (city) {
											obj.city = city;
										}
										if (country) {
											obj.country = country;
										}
										if (title) {
											obj.title = title;
										}
										if (industryObject.name) {
											obj.industry = industryObject;
										}
                                        $.ajax({
                                            type: "Post",
                                            url: serviceUrl+ "contact-v3.2/add?token=" + result.token + "&languageCode=en",
                                            dataType: 'json',
                                            contentType: "application/json",
                                            async: false,
                                            data: JSON.stringify(obj),
                                            success: function(result) {
                                                console.log(result);
                                                $(that).replaceWith("<a href='#' class='prospector-search-added'>Added</a>");
                                            },
                                            error: function(e) {
                                                console.log(e);
                                            },
                                        });
                                    }
                                });

                            },
                            error: function(error) {
                                console.log(error);
                            }
                        });
                    });

                },
                error: function(e) {
                    console.log(e);
                },
            });
        }
    });

}).on("click", ".add-all", function(e) {
    e.preventDefault();

    var that = this;
    var fetchables = [];
    localStorage['linked-list2'] = prospector_box.find("#listname").val();
    prospector_box.find(".prospector-search-person a").each(function(i, button) {
        fetchables.push(new Object({
            type: "linkedin_user_profile",
            url: $(button).get(0).href,
            description: $(button).data('name')
        }));

        // $.get($(button).get(0).href, function(data){
        // console.log($(data).find(".profile-card-extras .public-profile"));
        // console.log($(data).find(".profile-card-extras .public-profile a").attr("href"));		
        // });

    });

    $(that).replaceWith("<div class='prospector-added'>All Added!</div>");
    prospector_box.find(".prospector-search-add a").remove();
    prospector_box.find(".prospector-search-add").append("<a href='#' class='prospector-search-added'>Added</a>");

});


prospector_box.on("click", ".prospector-search-company a", function(event) {
    var that = this;
    event.preventDefault();
    event.stopPropagation();
    chrome.storage.local.get(["loginInd", "token"], function(result) {
        loginInd = result.loginInd;
        if (loginInd == 'false') {
            displayErrorLoginModal();
        } else {

            //Call API to add prospect
            var href = $(that).attr('href');
            var company = $('<div/>').html(that.outerHTML).contents().attr("data-name");
            var industry = $('<div/>').html(that.outerHTML).contents().attr("data-headline");
            var location = $('<div/>').html(that.outerHTML).contents().attr("data-location");
            var image = $('<div/>').html(that.outerHTML).contents().attr("data-image");
            var size = $('<div/>').html(that.outerHTML).contents().attr("data-size");
            if (size)
                size = size.replace('employees', '').trim();
            console.log(size);
            var city = '',
                country = '';
            if (location.split(",").length > 1) {
                city = location.split(",")[0];
                country = location.split(",")[1];
            } else {
                country = location;
            }

            $.ajax({
                type: "Get",
                url: serviceUrl+ "administration-v3.2/workData/workData/industries?token=" + result.token,
                success: function(data) {
                    var industryObject = {};
                    $.each(data.workDataOrganisationDTOList, function(i, obj) {
                        if (obj.name === industry) industryObject = obj;
                    });
                    $.ajax({
                        type: "Get",
                        url: serviceUrl + "administration-v3.2/workData/workData/getSizeTypes?token=" + result.token,
                        success: function(data) {
                            var sizeObject = {};
                            $.each(data.sizeTypeList, function(i, obj) {
                                if (obj.name === size) sizeObject = obj;
                            });
                            var avatarImage = '';
                            // upload image
                            toDataUrl(image, function(base64Img) {
                                var dd = getdata(base64Img);
                                var apiData = new FormData();
                                apiData.append("avatar", dd);
                                $.ajax({
                                    type: "POST",
                                    url: serviceUrl+  "document-v3.2/photo/uploadAvatar?token=" + result.token,
                                    data: apiData,
                                    mimeType: "multipart/form-data",
                                    dataType: 'json',
                                    contentType: false,
                                    async: true,
                                    processData: false,
                                    success: function(dataCheck) {
                                        avatarImage = dataCheck.avatar;
                                        var obj = {
                                            "name": company,                                         
                                            "avatar": avatarImage,                                           
                                            "mediaType": "LINKEDIN",
                                            "additionalEmailList": [],
                                            "additionalPhoneList": [],
                                            "isPrivate": false,
                                            "isChanged": false,
                                            "participantList": [],
                                            "numberGoalsMeeting": 0
                                        }
										if (city) {
											obj.city = city;
										}
										if (country) {
											obj.country = country;
										}
										if (sizeObject.name) {
											obj.size = sizeObject;
										}
										if (industryObject.name) {
											obj.industry = industryObject;
										}
                                        $.ajax({
                                            type: "Post",
                                            url: serviceUrl + "organisation-v3.2/add?token=" + result.token,
                                            dataType: 'json',
                                            contentType: "application/json",
                                            async: false,
                                            data: JSON.stringify(obj),
                                            success: function(result) {

                                                $(that).replaceWith("<a href='#' class='prospector-search-added'>Added</a>");
                                            },
                                        });
                                    },
                                    error: function(error) {
                                        console.log(error);
                                    }
                                });
                            });
                        }
                    });


                },
                error: function(e) {
                    console.log(e);
                },
            });

        }
    });

});



function getdata(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);

        return new Blob([raw], {
            type: contentType
        });
    }

    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {
        type: contentType
    });
}


function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.crossDomain = "true",
        xhr.onload = function() {
            var reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
    xhr.open('GET', url);
    xhr.send();
}
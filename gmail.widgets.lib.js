/**
 * Helper lib for hatchbuck widget extensions
 */
var pickathomeWidgets = (function () {
    var pickathome = {
        CONST_WIDGET_DOMAIN: "https://salesbox.com"
    };

    pickathome.listenLocationChanges = function(callback) {
        var storedLocation = window.location.href;
        intervalId = window.setInterval(function () {
            if (window.location.href !== storedLocation) {
                storedLocation = window.location.href;
                callback();
            }
        }, 100);
    }
    pickathome.listenEmailEntry = function(callback) {
        intervalId = window.setInterval(function () {

            callback();

        }, 100);
    }
    return pickathome;
}());
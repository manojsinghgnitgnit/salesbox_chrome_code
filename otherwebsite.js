var serviceUrl = "https://production-qa.salesbox.com/";


$("body").before("<div id='prospector-box-toggle'  > <a href = '#' class='sales-box-icon' ></a></div>");
$(document).ready(function() {
var prospector_box = $(
    "<div id='prospector-box' class='show'  >" +
    "<div style='padding: 10px 15px;'><a href='https://go.salesbox.com/' target='_blank' class='salebox-logo'></a>" +
    "<a href='#' id='hide-prospector-box'>-</a> <a href='#' id='hide-prospector-remove'>x</a></div>" +
    "<div id='gmaildata' style='text-align:center;'><div style='text-align:center;margin-top:200px'>Please select email address or contact information to add into salesbox.</div></div>" +
    "</div>"
);


	
    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
            chrome.storage.local.set({
                'closebox': 'true'
            });
            chrome.storage.local.set({
                'minimizebox': 'false'
            });
        });
    /* Build Prospector Box */
    chrome.storage.local.get(["minimizebox", "closebox"], function(result) {
        var closebox = result.closebox;
        var minimizebox = result.minimizebox;
        $("body").append(prospector_box);
        console.log(result);
        if (closebox == "true") {
            $("#prospector-box").hide();
            $("#prospector-box").removeClass('show');
        }
        if (minimizebox == "true") {
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
        } else {
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
        }
        $("#hide-prospector-box").on("click", function(event) {
            event.preventDefault();

            prospector_box.fadeOut();
            $("#prospector-box").removeClass('show');
            $("#prospector-box").hide();
            $("#prospector-box-toggle").show();
            $("#prospector-box-toggle").addClass('show');
            chrome.runtime.sendMessage({
                minimizebox: "minimizebox"
            });
        });
        $("#hide-prospector-remove").on("click", function(event) {
            event.preventDefault();

            prospector_box.fadeOut();
            $("#prospector-box").hide();
            $("#prospector-box").removeClass('show');
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
            chrome.runtime.sendMessage({
                closebox: "closebox"
            });
        });
        $("#prospector-box-toggle a").on("click", function(event) {
            event.preventDefault();
            chrome.runtime.sendMessage({
                toggle: "toggle"
            });
            $("#prospector-box-toggle").hide();
            $("#prospector-box-toggle").removeClass('show');
            $("#prospector-box").show();
            $("#prospector-box").addClass('show');
            prospector_box.fadeIn();
        });
        prospector_box.on("click", "#add-to-prospector", function(event) {
            event.preventDefault();
            event.stopPropagation();
            addToProspector();
        });
        $(document).bind("mouseup", function(e) {
            if ($(e.target).closest("#prospector-box").length > 0) {
                return false;
            } else {
                gText(e);
            }
        });
    });




    function gText(e) {
        var t = '';
        var email;
        var regexPhone;
        var phone;
        var firstName;
        var lastName;
        var address;
        var city;
        var name;
        var zip;
        var country;
        var title;
        t = getSelectionHtml(); // (document.all) ? document.selection.createRange().html : document.getSelection();           
        name = $("#selection_html_placeholder").find("[class*=name],[class*=hitlistLink]").first().text();
		 console.log(name);
        if (name) {
			name = name.replace(/[0-9]/g, '').replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').trim();
            name = name.split(' ');
            if (name.length > 1) {
                firstName = name[0];
                lastName = name[1];
            } else {
                firstName = name[0];
            }
        }
       
        var street_address = $("#selection_html_placeholder").find("[class*=street]").first().text();
        if (street_address) {
            address = street_address;
        }
        var phone_number = $("#selection_html_placeholder").find("[class*=phone-number],[class*=mobile],[class*=text11grey6]").first().text();
		
        if (phone_number) {
			phone = phone_number.replace ( /[^0-9]/g, '' );
             
        }
        var zipCode = $("#selection_html_placeholder").find("[class*=postal],[class*=zipcode]").first().text();
        if (zipCode) {
            zip = zipCode;
        }
        var location = $("#selection_html_placeholder").find("[class*=locality]").first().text();
        if (location) {
            city = location;
        }
        var country_name = $("#selection_html_placeholder").find("[class*=country]").first().text();
        if (country_name) {
            country = country_name;
        }
        var position = $("#selection_html_placeholder").find("[class*=title],[class*=position]").first().text();
        if (position) {
            title = position;
        }
        $('#selection_html_placeholder').remove();

        if (checkIfEmailInString(t)) {
            email = getEmailInString(t)[0];
            if (checkIfPhoneInString(t) == '1') {
                var regexPhone = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
                phone = getPhoneInString(t, regexPhone);
            } else if (checkIfPhoneInString(t) == '2') {
                var regexPhone = /\b\d{2}[-.]?\d{3}[-.]?\d{3}[-.]?\d{4}\b/g;
                phone = getPhoneInString(t, regexPhone);
            }
            if (isEmail(email)) {
                console.log('Email Found');
                chrome.storage.local.get(["loginInd", "token"], function(result) {
                    loginInd = result.loginInd;
                    if (loginInd == 'false') {
                        $("#gmaildata").html("<div style='text-align:center;margin-top:200px'>Please login into Salesbox chrome extension!</div>");
                    } else {
                        //Call API to add prospect
                        $.ajax({
                            type: "GET",
                            url: serviceUrl+ "contact-v3.2/getDetailsFromEmail?token=" + result.token + "&email=" + email + "&languageCode=en",
                            success: function(data) {
                                getProfile(result.token, data);
                            },
                            statusCode: {
                                500: function() {
                                    // If email not exist into salesbox data base show add contact form
                                    $("#gmaildata").load(chrome.extension.getURL("add.html"), function() {
									$("#salesbox-plugin").css("height","510px");
                                        localStorage["ActivityID"] = "2";
                                        $("#email").val(email);
                                        // bind Account
                                        bindAccount(result.token);
                                        //  bind country
                                        bindCountry(result.token);
                                        //bind type
                                        bindType(result.token);
                                        // Bind Industry
                                        bindIndustry(result.token);
                                        // Bind Relation
                                        bindRelation(result.token);
                                        $("[data-toggle='dropdown']").on('click', function() {
                                            var dropdown = $(this).siblings(".dropdown-menu");
                                            if (dropdown.is(":hidden")) {
                                                $(".dropdown-menu").slideUp();
                                                dropdown.slideDown();
                                            }
                                        });
                                        $("[data-toggle='form-group-addon']").on('click', function() {
                                            var target = $(this).attr('data-target');
                                            $(target).removeClass("hidden");
                                        });
                                        $("#relationshiplist a").on('click', function() {
                                            $(this).closest(".dropdown-menu").slideUp();
                                            $("#relationship").val($(this).text().trim());
                                        });
                                        $("#behaviourlist a").on('click', function() {
                                            $(this).closest(".dropdown-menu").slideUp();
                                            $("#behaviour").val($(this).text().trim());
                                        });
                                    });
                                }
                            },
                            error: function(e) {
                                console.log(e);
                            },
                        });

                    }
                });
            } else {

                chrome.storage.local.get(["loginInd", "token"], function(result) {
                    loginInd = result.loginInd;
                    if (loginInd == 'false') {
                        $("#gmaildata").html("<div style='text-align:center;margin-top:200px'>Please login into Salesbox chrome extension!</div>");
                    } else {
                        $("#gmaildata").load(chrome.extension.getURL("add.html"), function() {
                            localStorage["ActivityID"] = "2";
							$("#salesbox-plugin").css("height","510px");
                            $("#email").val(email);
                             
                            $("#firstname").val(firstName);
                            $("#lastname").val(lastName);
                            $("#phone").val(phone);
                            $("#street").val(address);
                            $("#zip").val(zip);
                            $("#city").val(city);
                            $("#country").val(country);
                            $("#title").val(title);
                            // bind Account
                            bindAccount(result.token);
                            //  bind country
                            bindCountry(result.token);
                            //bind type
                            bindType(result.token);
                            // Bind Industry
                            bindIndustry(result.token);
                            // Bind Relation
                            bindRelation(result.token);
                            $("[data-toggle='dropdown']").on('click', function() {
                                var dropdown = $(this).siblings(".dropdown-menu");
                                if (dropdown.is(":hidden")) {
                                    $(".dropdown-menu").slideUp();
                                    dropdown.slideDown();
                                }
                            });
                            $("[data-toggle='form-group-addon']").on('click', function() {
                                var target = $(this).attr('data-target');
                                $(target).removeClass("hidden");
                            });
                            $("#relationshiplist a").on('click', function() {
                                $(this).closest(".dropdown-menu").slideUp();
                                $("#relationship").val($(this).text().trim());
                            });
                            $("#behaviourlist a").on('click', function() {
                                $(this).closest(".dropdown-menu").slideUp();
                                $("#behaviour").val($(this).text().trim());
                            });
                        });
                    }
                });
            }
        } else {
            console.log(firstName);
            if (firstName) {
                chrome.storage.local.get(["loginInd", "token"], function(result) {
                    loginInd = result.loginInd;
                    if (loginInd == 'false') {
                        $("#gmaildata").html("<div style='text-align:center;margin-top:200px'>Please login into Salesbox chrome extension!</div>");
                    } else {
                        $("#gmaildata").load(chrome.extension.getURL("add.html"), function() {
						$("#salesbox-plugin").css("height","510px");
                            localStorage["ActivityID"] = "2";
							console.log("Contact Add");
                            $("#email").val(email);
                            $("#firstname").val(firstName);
                            $("#lastname").val(lastName);
                            $("#phone").val(phone);
                            $("#street").val(address);
                            $("#zip").val(zip);
                            $("#city").val(city);
                            $("#country").val(country);
                            $("#title").val(title);
                            // bind Account
                            bindAccount(result.token);
                            //  bind country
                            bindCountry(result.token);
                            //bind type
                            bindType(result.token);
                            // Bind Industry
                            bindIndustry(result.token);
                            // Bind Relation
                            bindRelation(result.token);
                            $("[data-toggle='dropdown']").on('click', function() {
                                var dropdown = $(this).siblings(".dropdown-menu");
                                if (dropdown.is(":hidden")) {
                                    $(".dropdown-menu").slideUp();
                                    dropdown.slideDown();
                                }
                            });
                            $("[data-toggle='form-group-addon']").on('click', function() {
                                var target = $(this).attr('data-target');
                                $(target).removeClass("hidden");
                            });
                            $("#relationshiplist a").on('click', function() {
                                $(this).closest(".dropdown-menu").slideUp();
                                $("#relationship").val($(this).text().trim());
                            });
                            $("#behaviourlist a").on('click', function() {
                                $(this).closest(".dropdown-menu").slideUp();
                                $("#behaviour").val($(this).text().trim());
                            });
                        });
                    }
                });

            }


        }

    }
    // document.onmouseup = gText;

});




function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function checkIfPhoneInString(text) {
    var regexPhone = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    if (regexPhone.test(text)) {
        return '1';
    } else {
        regexPhone = /\b\d{2}[-.]?\d{3}[-.]?\d{3}[-.]?\d{4}\b/g;
        if (regexPhone.test(text)) {
            return '2';
        }
    }
    return false;
}

function checkIfEmailInString(text) {
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
}

function getEmailInString(text) {
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    var re1 = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;
    return re1.exec(text);
}

function getPhoneInString(text, regexPhone) {
    return regexPhone.exec(text);
}

function getSelectionHtml() {
    var htmlContent = ''

    var range = window.getSelection().getRangeAt(0);
    var content = range.cloneContents();
    $('#selection_html_placeholder').remove();
    $('body').append('<span id="selection_html_placeholder"></span>');
    var placeholder = document.getElementById('selection_html_placeholder');

    placeholder.appendChild(content);

    htmlContent = placeholder.innerHTML;
    //$('#selection_html_placeholder').remove();
    //var html = $.parseHTML(htmlContent);
	console.log(htmlContent);
    return htmlContent;
}

function addToProspector() {
    var btn = $("#add-to-prospector");
    var fname = $("#firstname").val();
    var lname = $("#lastname").val();
    if (fname == '') {
        $("#firstname").css('background', 'rgba(255, 231, 224, 0.498039)');
        return;
    } else {
        $("#firstname").css('background', '');
    }
    if (lname == '') {
        $("#lastname").css('background', 'rgba(255, 231, 224, 0.498039)');
        return;
    } else {
        $("#lastname").css('background', '');
    }

    var contact = {
        "firstName": fname,
        "lastName": lname,
        "discProfile": "NONE",
        "additionalEmailList": [],
        "additionalPhoneList": [],
        "relationship": "YELLOW",
        "participantList": [],
        "mediaType": "MANUAL"
    };
    if ($("#street").val()) {
        contact.street = $("#street").val();
    }
    if ($("#zip").val()) {
        contact.zipCode = $("#zip").val();
    }
    if ($("#city").val()) {
        contact.city = $("#city").val();
    }
    if ($("#country").val()) {
        contact.country = $("#country").val();
    }
    if ($("#title").val()) {
        contact.title = $("#title").val();
    }
    if ($("#email").val()) {
        contact.title = $("#email").val();
        contact.mainEmailType = "EMAIL_WORK";		
		contact.additionalEmailList = [ {"value": $("#email").val() ,"type":"EMAIL_WORK","main":true,"isPrivate":false}];
    }
    if ($("#phone").val()) {
        contact.phone = $("#phone").val();
        contact.mainPhoneType = "PHONE_WORK";		
		contact.additionalPhoneList = [ {"value": $("#phone").val() ,"type":"PHONE_WORK","main":true,"isPrivate":false}];
    }
    if ($("#account").attr('data-uuid')) {
        contact.organisationId = $("#account").attr('data-uuid');
    }
    if ($("#type").attr('data-uuid')) {
        contact.type = {
            "uuid": $("#type").attr('data-uuid'),
            "type": $("#type").attr('data-type'),
            "name": $("#type").attr('data-name'),
            "code": $("#type").attr('data-code')
        };
    }
    if ($("#industry").val()) {
        contact.industry = {
            "uuid": $("#industry").attr('data-uuid'),
            "type": $("#industry").attr('data-type'),
            "name": $("#industry").attr('data-name'),
            "code": $("#industry").attr('data-code')
        };
    }
    if ($("#relation").attr('data-uuid')) {
        contact.relation = {
            "uuid": $("#relation").attr('data-uuid'),
            "type": $("#relation").attr('data-type'),
            "name": $("#relation").attr('data-name'),
            "code": $("#relation").attr('data-code')
        };
    }


    chrome.storage.local.get(["loginInd", "token"], function(result) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(contact),
            url: serviceUrl + "contact-v3.2/add?token=" + result.token + "&languageCode=en",
            success: function(data) {
                if (data.email) {
                    $.ajax({
                        type: "GET",
                        url: serviceUrl+ "contact-v3.2/getDetailsFromEmail?token=" + result.token + "&email=" + data.email + "&languageCode=en",
                        success: function(data) {
                            getProfile(result.token, data);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } else {
                    $("#gmaildata").html("<div style='text-align:center;margin-top:200px'>Contact has been added successfully!</div>");
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
}

function getProfile(token, data) {
    console.log(data);
    $("#gmaildata").load(chrome.extension.getURL("detail.html"), function() {
        $("#pname").text(data.firstName + ' ' + data.lastName);
        var headline = '';
        if (data.title && data.organisationName)
            headline = data.title + ' at ' + data.organisationName;
        else if (data.title)
            headline = data.title;
        else if (data.organisationName)
            headline = data.organisationName;
        if (data.relation)
            headline = headline + " (" + data.relation.name + ")";
        $("#headline").text(headline);

        if (data.industry && data.type)
            $("#industry").text(data.type.name + ' in ' + data.industry.name);
        else if (data.type)
            $("#industry").text(data.type.name);
        else if (data.industry)
            $("#industry").text(data.industry.name);
        if (data.phone)
            $("#phone").text(data.phone);
        if (data.email)
            $("#email").text(data.email);
        var address = '';
        if (data.street)
            address = address + data.street + ",";
        if (data.city)
            address = address + data.city + ",";
        if (data.country)
            address = address + data.country + "-";
        if (data.street)
            address = address + data.zipCode;
        $("#address").text(address);
        if (data.avatar) {
            var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
            var folder = data.avatar.substring(data.avatar.length - 3, data.avatar.length);
            $("#profileimage").attr('src', imageCdn + folder + '/' + data.avatar);
        }
        //  Sales
        $("#dealsize").text(data.medianDealSize);
        $("#dealtime").text(data.medianDealTime + ' Days');
        $("#totalsales").text(data.orderIntake);
        $("#salemargin").text(data.closedMargin + ' %');
        $("#saleprofilt").text('Profit :' + data.wonProfit);

        // pipe
        $("#grasspipe").text(data.grossPipeline);
        $("#weightedpipe").text(data.netPipeline);
        $("#pipeprofit").text(data.pipeProfit);
        $("#pipemargin").text(data.pipeMargin + '%');
        //statistics
        $("#dials").text(data.numberCall);
        $("#calls").text(data.numberPick);
        $("#appointment").text(data.numberFinishedMeeting);
        // Last communication
        var lastCommunication = '';
        var pubDate = '';
        $.each(data.latestCommunicationHistoryDTOList, function(i, obj) {
            pubDate = new Date(obj.startDate);
            lastCommunication = lastCommunication + ' <tr> <td> <p>' + obj.userName + '</p><p>' + obj.type + '</p></td><td><p class="text-right">' + pubDate.toString("hh:mm tt") + ',' + pubDate.toString("dd MMM,yyyy") + '</p><p class="text-right">' + obj.duration + '</p></td> </tr>';
        });
        $("#lastCommunication").append(lastCommunication);
        var participant = '';
        $("#contactcount").text('Contact team (' + data.participantList.length + ')');
        var imagescr = 'https://placehold.it/60x60';
        $.each(data.participantList, function(i, obj) {
            if (obj.avatar) {
                var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                var folder = obj.avatar.substring(obj.avatar.length - 3, obj.avatar.length);
                imagescr = imageCdn + folder + '/' + obj.avatar;
            }
            participant = participant + '<div class="row"><div class="col-xs-3"><img class="img-circle avatar-medium" src="' + imagescr + '"></div>' +
                '<div class="col-xs-9"><table class="table contact-team-table"><tbody>' +
                '<tr><td class="width-20">&nbsp;</td><td class="width-60"><strong class="color-red">' + obj.firstName + ' ' + obj.lastName + '</strong></td><td class="width-10">50%</td><td class="width-10"><!-- ngIf: contact.isSystemContact == false --></td></tr>' +
                '<tr><td class="text-center"> <i class="fa-envelope-o icon"></i> </td><td><a href="javascript:void(0)">' + obj.email + '</a> </td><td> <a href="#" class="contact-team-action color-green"><i class="fa fa-check-circle"></i></a></td><td><a href="javascript:void(0)" class="contact-team-action color-red"><i class="fa fa-times-circle"></i></a></td></tr>' +
                '<tr> <td class="text-center"><i class="text-14 fa fa-phone icon"></i></td><td colspan="3"><a href="javascript:void(0)">' + obj.phone + '</a></td></tr>' +
                '</tbody></table></div></div>';
        });
        $("#participantlist").append(participant);
        // Leads
        getLead(token, data.uuid);
        //Appointment
        getAppointment(token, data.uuid);
        getOpportunity(token, data.uuid);
        getTask(token, data.uuid)
        getClosed(token, data.uuid);
        getNote(token, data.uuid)
        if (data.organisationId)
            getColleagues(token, data.organisationId);
    });
}

function getLead(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "lead-v3.2/listByContactAndYear?token=" + token + "&contactId=" + contactid,
        success: function(data) {
            var lead = '';
            $("#lead .badge").text(data.leadDTOList.length);
            $(".color-lead").text(data.leadDTOList.length);
            $.each(data.leadDTOList, function(i, obj) {
                var pubDate = new Date(obj.createdDate);
                var priority = '';
                if (obj.priority <= 20) priority = "fa fa-thermo-1";
                if (obj.priority > 20 && obj.priority <= 40) priority = "fa fa-thermo-2";
                if (obj.priority > 40 && obj.priority <= 60) priority = "fa fa-thermo-3";
                if (obj.priority > 60 && obj.priority <= 80) priority = "fa fa-thermo-4";
                if (obj.priority > 80 && obj.priority <= 100) priority = "fa fa-thermo-5";
                var imagescr = 'https://placehold.it/40x40';
                if (obj.creatorAvatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.creatorAvatar.substring(obj.creatorAvatar.length - 3, obj.creatorAvatar.length);
                    imagescr = imageCdn + folder + '/' + obj.creatorAvatar;
                }
                lead = lead + '<tr><td width="15%"><div class="lead-icon"><i class="fa fa-clock color-red"></i><i class="' + priority + '"></i></div></td>' +
                    '<td width="20%">' + pubDate.toString("dd MMM, yyyy hh:mm") + ' </td><td width="50%"><p class="contact">' + obj.contactFirstName + ' ' + obj.contactLastName + '</p><p class="account">' + obj.organisationName + '</p></td>' +
                    '<td width="5%"><a href="javascript:void(0);" data-toggle="popover" data-content="<p><i class=\'fa fa-phone\'></i> ' + obj.contactPhone + '</p><p><i class=\'fa fa-envelope\'></i> ' + obj.contactEmail + '</p>" data-placement="top" data-trigger="hover" data-html="true" data-original-title="" title=""><i class="fa fa-info-circle info-icon"></i> </a></td>' +
                    '<td width="10%" class="text-center"><img class="avatar-small img-circle" src="' + imagescr + '"></td></tr>';
            });
            $("#leadlist").append(lead);
        }
    });
}
function getAppointment(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "appointment-v3.2/syncByContact?&contactId=" + contactid + "&showHistory=false&token=" + token,
        success: function(data) {
            var appointment = '';
            $("#appointments .badge").text(data.appointmentDTOList.length);
            $(".color-appointment").text(data.appointmentDTOList.length);
            $.each(data.appointmentDTOList, function(i, obj) {
                var startdt = new Date(obj.startDate);
                var enddt = new Date(obj.endDate);
                var imagecontact = 'https://placehold.it/40x40';
                if (obj.contactList.length > 0 && obj.contactList[0].avatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.contactList[0].avatar.substring(obj.contactList[0].avatar.length - 3, obj.contactList[0].avatar.length);
                    imagecontact = imageCdn + folder + '/' + obj.contactList[0].avatar;
                }
                var imageowner = 'http://placehold.it/40x40';
                if (obj.owner.avatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.owner.avatar.substring(obj.owner.avatar.length - 3, obj.owner.avatar.length);
                    imageowner = imageCdn + folder + '/' + obj.owner.avatar;
                }
                var focus = '';
                if (obj.focusWorkData) {
                    focus = obj.focusWorkData.name;
                }
                appointment = appointment + '<tr><td class="timeline-badge" width="5%">&nbsp;</td>' +
                    '<td width="15%"><img class="avatar-small img-circle" src="' + imagecontact + '"></td>' +
                    '<td width="20%"><p class="focus">' + focus + '</p></td>' +
                    '<td width="25%">' + startdt.toString("dd MMM,yyyy") + ', ' + startdt.toString("hh:mm") + ' ' + enddt.toString("hh:mm") + '</td><td width="20%"><p>' + obj.location + '</p></td>' +
                    '<td width="15%" class="text-center"><img class="avatar-small img-circle" src="' + imageowner + '" ></td></tr>';
            });
            $("#appointmentslist").append(appointment);
        }
    });
}

function getOpportunity(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "prospect-v3.2/listByContact/" + contactid + "?token=" + token,
        success: function(data) {		
            var opportunity = '';
            $("#opportunities .badge").text(data.prospectDTOList.length);
            $(".color-opportunity").text(data.prospectDTOList.length);
            $.each(data.prospectDTOList, function(i, obj) {
                var imagescr = 'https://qa.salesbox.com/desktop/assets/img/non-sprite/Gray_Photo.png';
                if (obj.ownerAvatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.ownerAvatar.substring(obj.ownerAvatar.length - 3, obj.ownerAvatar.length);
                    imagescr = imageCdn + folder + '/' + obj.ownerAvatar;
                }

                opportunity = opportunity + '<tr><td width="20%"> <svg width="54" height="54" viewBox="0 0 54 54" preserveAspectRatio="xMinYMin" style="margin: 0px auto;"><g transform="translate(27,27)"><circle r="27" fill="transparent"></circle>' +
                    '<circle r="21" fill="#173849"></circle><text text-anchor="middle" class="" dy="5" dx="0" style="fill: rgb(255, 255, 255); font-size: 15px;">0%</text>' +
                    '<path d="M0,27A27,27 0 1,1 0,-27A27,27 0 1,1 0,27M0,23.400000000000002A23.400000000000002,23.400000000000002 0 1,0 0,-23.400000000000002A23.400000000000002,23.400000000000002 0 1,0 0,23.400000000000002Z" style="fill: rgb(225, 86, 86);"></path></g></svg></td>' +
                    '<td width="30%"><p class="account">' + obj.organisation.name + '</p> <p>' + obj.description + '</p></td>' +
                    '<td width="10%" class="text-center"><strong>' + obj.grossValue + '</strong></td>' +
                    '<td width="10%" class="text-center"><strong>' + obj.netValue + '</strong></td>' +
                    '<td width="10%"></td>' +
                    '<td width="20%" class="text-center"><img class="avatar-small img-circle" src="' + imagescr + '"></td></tr>';
            });
            $("#opportunitieslist").append(opportunity);
        }
    });
}

function getTask(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "task-v3.2/listByContact?contactId=" + contactid + "&orderBy=dateAndTime&pageIndex=0&pageSize=45&showHistory=false&token=" + token,
        success: function(data) {
            var task = '';
            $("#task .badge").text(data.taskDTOList.length);
            $(".color-task").text(data.taskDTOList.length);
            $.each(data.taskDTOList, function(i, obj) {
                var imagescr = 'https://placehold.it/40x40';
                if (obj.ownerAvatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.ownerAvatar.substring(obj.ownerAvatar.length - 3, obj.ownerAvatar.length);
                    imagescr = imageCdn + folder + '/' + obj.ownerAvatar;
                }
                var focus = '';
                if (obj.focusWorkData) {
                    focus = obj.focusWorkData.name;
                }
                task = task + '<tr><td class="border-task-green" width="20%">07 May, 2016 17:15</td>' +
                    '<td width="25%"> <p class="contact">' + obj.contactName + '</p><p class="account">' + obj.organisationName + '</p></td>' +
                    '<td width="5%"><a href="#" data-toggle="popover" data-content="<p><i class=\'fa fa-phone\'></i> ' + obj.contactPhone + '</p><p><i class=\'fa fa-envelope\'></i> ' + obj.contactEmail + '</p>"data-placement="top" data-trigger="hover" data-html="true"> <i class="fa fa-info-circle info-icon"></i></a></td>' +
                    '<td width="25%"> <p class="focus">' + focus + '</p><p class="category">' + obj.categoryName + '</p></td>' +
                    '<td><a href="#" class="check-icon"><i class="fa fa-check-circle"></i></a></td>' +
                    '<td width="20%" class="text-center"><img class="avatar-small img-circle" src="' + imagescr + '"></td></tr>';
            });
            $("#tasklist").append(task);
        }
    });
}

function getColleagues(token, organisationId) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "contact-v3.2/listByOrganisation?&customFilter=active&organisationId=" + organisationId + "&token=" + token,
        success: function(data) {
            var colleagues = '';
            $("#colleagues .badge").text(data.contactDTOList.length);
            $(".color-contact").text(data.contactDTOList.length);
            $.each(data.contactDTOList, function(i, obj) {
                var imagescr = 'https://placehold.it/40x40';

                if (obj.avatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.avatar.substring(obj.avatar.length - 3, obj.avatar.length);
                    imagescr = imageCdn + folder + '/' + obj.avatar;
                }

                var ownerscr = 'https://placehold.it/40x40';
                if (obj.ownerAvatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.ownerAvatar.substring(obj.ownerAvatar.length - 3, obj.ownerAvatar.length);
                    ownerscr = imageCdn + folder + '/' + obj.ownerAvatar;
                }
                colleagues = colleagues + '<tr><td width="20%"><div class="relationship-ring border-relationship-yellow"><img class="avatar-small img-circle" src="' + imagescr + '"></div></td>' +
                    '<td width="30%"><p class="contact">' + obj.firstName + ' ' + obj.lastName + '</p><p class="account">' + obj.organisationName + '</p></td>' +
                    '<td width="5%" class="text-center">' + obj.participantList.length + '</td>' +
                    '<td width="5%" class="text-center">' + obj.numberActiveTask + '</td>' +
                    '<td width="5%" class="text-center">' + obj.numberActiveMeeting + '</td>' +
                    '<td width="5%" class="text-center">' + obj.numberActiveProspect + '</td>' +
                    '<td width="20%" class="text-center"><img class="avatar-small img-circle" src="' + ownerscr + '"></td></tr>';
            });

            $("#colleagueslist").append(colleagues);
        }
    });
}

function getClosed(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "prospect-v3.2/listClosedByContactFull/" + contactid + "?token=" + token,
        success: function(data) {
            var closed = '';
            $("#closed .badge").text(data.prospectDTOList.length);
            $(".color-closed").text(data.prospectDTOList.length);
            var grass = 0;
            var profit = 0;
            $.each(data.prospectDTOList, function(i, obj) {
                var imagescr = 'https://placehold.it/40x40';
                if (obj.participantList[0].avatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.participantList[0].avatar.substring(obj.participantList[0].avatar.length - 3, obj.participantList[0].avatar.length);
                    imagescr = imageCdn + folder + '/' + obj.participantList[0].avatar;
                }
                grass = grass + obj.grossValue;
                profit = profit + obj.profit;
                var status = "";
                if (obj.won == "true")
                    status = "color-green";
                else
                    status = "color-red";
                closed = closed + '<tr><td width="20%"><i class="fa fa-star-circle-ban won-lost-icon ' + status + '"></i> </td> <td width="30%"><p class="account">' + obj.sponsorList[0].firstName + ' ' + obj.sponsorList[0].lastName + '</p><p>' + obj.description + '</p></td>' +
                    '<td width="15%" class="text-center"><strong>' + obj.grossValue + '</strong></td><td width="15%" class="text-center"><strong>' + obj.profit + '</strong> </td>' +
                    '<td width="20%" class="text-center"><img class="avatar-small img-circle" src="' + imagescr + '"></td></tr>';
            });
            $("#grass").text("Gross: " + grass);
            $("#profit").text("profit: " + profit);
            $("#closedlist").append(closed);
        }
    });
}

function getNote(token, contactid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "document-v3.2/note/listByContactFull/" + contactid + "?pageIndex=0&pageSize=45&token=" + token,
        success: function(data) {
            var note = '';
            $("#note .badge").text(data.noteDTOList.length);
            $(".color-note").text(data.noteDTOList.length);
            $.each(data.noteDTOList, function(i, obj) {
                var imagescr = 'https://placehold.it/40x40';
                if (obj.authorAvatar) {
                    var imageCdn = 'https://ams01.objectstorage.softlayer.net/v1/AUTH_a5a91c99-65b2-42f7-b169-14a0542e9730/salesboxfiles/';
                    var folder = obj.authorAvatar.substring(obj.authorAvatar.length - 3, obj.authorAvatar.length);
                    imagescr = imageCdn + folder + '/' + obj.authorAvatar;
                }
                var createddt = new Date(obj.createdDate);
                note = note + '<tr><td width="20%" class="text-center"><img class="avatar-small img-circle" src="' + imagescr + '">' +
                    '<p> ' + createddt.toString("hh:mm     dd MMM, yyyy") + '</p></td><td width="80%"> <div class="note"><p class="subject">' + obj.subject + '</p><p>' + obj.content + '</p></div></td></tr>';
            });
            $("#notelist").append(note);
        }
    });
}

function bindType(token) {
    var type = '';
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "administration-v3.2/workData/organisations?token=" + token,
        success: function(data) {
            console.log(data.workDataOrganisationDTOList);
            $.each(data.workDataOrganisationDTOList, function(i, obj) {
                type = type + '<li><a class="" href="javascript:void(0)" data-uuid="' + obj.uuid + '" data-type="' + obj.type + '" data-name="' + obj.name + '" data-code="' + obj.code + '">' + obj.name + '</a></li>';
            });
            $("#typelist").append(type);
            $("#typelist a").on('click', function() {
                $(this).closest(".dropdown-menu").slideUp();
                $("#type").val($(this).text());
                $("#type").attr('data-uuid', $(this).attr('data-uuid'));
                $("#type").attr('data-type', $(this).attr('data-type'));
                $("#type").attr('data-name', $(this).attr('data-name'));
                $("#type").attr('data-code', $(this).attr('data-code'));
            });
        }
    });
}

function bindIndustry(token) {
    var industry = '';
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl+ "administration-v3.2/workData/workData/industries?token=" + token,
        success: function(data) {
            console.log(data.workDataOrganisationDTOList);
            $.each(data.workDataOrganisationDTOList, function(i, obj) {
                industry = industry + '<li><a class="" href="javascript:void(0)" data-uuid="' + obj.uuid + '" data-type="' + obj.type + '" data-name="' + obj.name + '" data-code="' + obj.code + '">' + obj.name + '</a></li>';
                if (i == 0) {
                    $("#industry").attr('data-uuid', obj.uuid);
                    $("#industry").attr('data-type', obj.type);
                    $("#industry").attr('data-name', obj.name);
                    $("#industry").attr('data-code', obj.code);
                }
            });
            $("#industrylist").append(industry);
            $("#industrylist a").on('click', function() {
                $(this).closest(".dropdown-menu").slideUp();
                $("#industry").val($(this).text());
                $("#industry").attr('data-uuid', $(this).attr('data-uuid'));
                $("#industry").attr('data-type', $(this).attr('data-type'));
                $("#industry").attr('data-name', $(this).attr('data-name'));
                $("#industry").attr('data-code', $(this).attr('data-code'));
            });
        }
    });
}

function bindRelation(token) {
    var relation = '';
    $.ajax({
        type: "GET",
        dataType: "json",
        url: serviceUrl + "administration-v3.2/workData/organisations?token=" + token,
        success: function(data) {
            console.log(data.workDataOrganisationDTOList);
            $.each(data.workDataOrganisationDTOList, function(i, obj) {
                relation = relation + '<li><a class="" href="javascript:void(0)" data-uuid="' + obj.uuid + '" data-type="' + obj.type + '" data-name="' + obj.name + '" data-code="' + obj.code + '">' + obj.name + '</a></li>';
            });
            $("#relationlist").append(relation);
            $("#relationlist a").on('click', function() {
                $(this).closest(".dropdown-menu").slideUp();
                $("#relation").val($(this).text());
                $("#relation").attr('data-uuid', $(this).attr('data-uuid'));
                $("#relation").attr('data-type', $(this).attr('data-type'));
                $("#relation").attr('data-name', $(this).attr('data-name'));
                $("#relation").attr('data-code', $(this).attr('data-code'));
            });
        }
    });
}

function bindAccount(token) {
    $("#account").autocomplete({
        minLength: 0,
        delay: 0,
        source: function(request, response) {
            $.ajax({
                type: "POST",
                crossDomain: true,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    "name": request.term
                }),
                url: serviceUrl+ "organisation-v3.2/searchLocal?token=" + token + "&updatedDate=0&pageIndex=0&pageSize=10",
                success: function(data) {
                    response($.map(data.organisationDTOList, function(item) {
                        return {
                            label: item.name,
                            value: item.uuid
                        }
                    }));
                }
            })
        },
        search: function(event, ui) {
            $('#accountlist li').remove();
        },
        focus: function(event, ui) {

        },
        select: function(event, ui) {
            return false;
        }
    }).focus(function() {
        //Use the below line instead of triggering keydown
        $(this).autocomplete("search");
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var data1 = $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a class='ui-menu-item' data-uuid=" + item.value + "  href='javascript:void(0)'>" + item.label + "</a>")
            .appendTo("#accountlist");
        $("#accountlist a").on('click', function(event) {
            event.stopImmediatePropagation();
            $("#account").val($(this).text());
            $("#account").attr('data-uuid', $(this).attr('data-uuid'));
            $(this).closest(".dropdown-menu").slideUp();
        });
        return data1;


    };
}

function bindCountry(token) {
    var country;
    $.ajax({
        type: "Get",
        async: false,
        url: serviceUrl+ "administration-v3.2/workData/workData/countries?token=" + token,
        success: function(data) {
            country = $.map(data.countryDTOList, function(item) {
                return {
                    label: item.name,
                    value: item.name
                }
            });
            console.log(country);
        },
        error: function(e) {
            console.log(e);
        },
    });


    $("#txt-country-search").autocomplete({
        minLength: 0,
        delay: 0,
        source: country,
        search: function(event, ui) {
            $('#countrylist li:not(:first)').remove();
        },
        focus: function(event, ui) {
            return false;
        },
        select: function(event, ui) {
            return false;
        }
    }).focus(function() {
        //Use the below line instead of triggering keydown
        $(this).autocomplete("search");
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var data1 = $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a class='ui-menu-item'   href='javascript:void(0)'>" + item.label + "</a>")
            .appendTo("#countrylist");
        $("#countrylist a").on('click', function(event) {
            event.stopImmediatePropagation();
            $("#country").val($(this).text());
            $(this).closest(".dropdown-menu").slideUp();
        });
        return data1;


    };
}
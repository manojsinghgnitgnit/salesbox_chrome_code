
if (localStorage['version'] != chrome.app.getDetails().version) {
    if (typeof localStorage['version'] == 'undefined') {
        //new install
        chrome.tabs.getAllInWindow(null, ensureInstallTab);
    } else {
        //update
        var oldVersion = localStorage['version'].split('.');
        if (oldVersion[0] == 1 && oldVersion[1] <= 1) {
            alert("New version has been updated!");
        }
    }
    localStorage['version'] = chrome.app.getDetails().version;
	chrome.storage.local.set({'loginInd': 'false'});
    localStorage['loggedIn'] = false;
}


   
 

chrome.storage.local.set({'loginInd': 'false'});  
if (localStorage['loggedIn']=="true") login();

function ensureInstallTab(tabs) {
  alert("You have installed successfully!");
}

function login() {
    localStorage['loggedIn'] = true;
	chrome.storage.local.set({'loginInd': 'true'});
    loggedIn = true; 
    chrome.browserAction.setPopup({popup: 'logout.html'}); 
 	
}

function logout() {
    loggedIn = false;   
	chrome.storage.local.set({'loginInd': 'false'});
    localStorage['loggedIn'] = false;    
    chrome.browserAction.setPopup({popup: 'login.html'}); 
   	
}
chrome.browserAction.onClicked.addListener(function(tab) {
    
	console.log("Calling");
});
 
chrome.extension.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.loggedIn) {    
        login();
	localStorage['token'] =  message.currentUserId;
	chrome.storage.local.set({'token': message.currentUserId });		
  } else if (message.loggedOut) {  
    logout();
  } 
  if (message.saveAccessToken) {
      localStorage['accessTokenGmail'] = message.accessTokenGmail;
      chrome.storage.local.set({'accessTokenGmail': message.accessTokenGmail });
  }
  if(message.closebox) {	  	
	 chrome.storage.local.set({'closebox': 'true'});
	 chrome.storage.local.set({'minimizebox': 'true'});
  }
  if(message.minimizebox) {   
   chrome.storage.local.set({'closebox': 'true'});
   chrome.storage.local.set({'minimizebox': 'false'});
  }
  if(message.toggle) {   
   chrome.storage.local.set({'closebox': 'false'});
   chrome.storage.local.set({'minimizebox': 'true'});
  }
});
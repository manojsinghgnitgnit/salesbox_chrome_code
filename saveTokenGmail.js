$(document).ready(function() {
    var startTime = new Date().getTime();
    var interval = setInterval(function(){
        if(new Date().getTime() - startTime > 5000){
            clearInterval(interval);
            return;
        }
        
        var accessTokenGmail = $('#access_token').html();    
        if ((typeof accessTokenGmail != 'undefined') && (accessTokenGmail.length > 0)) {
            chrome.runtime.sendMessage({
                saveAccessToken: true,
                accessTokenGmail: accessTokenGmail,
            });
        }
    }, 1000);              
});